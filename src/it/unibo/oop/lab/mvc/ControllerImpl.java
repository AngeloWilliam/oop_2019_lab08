/**
 * 
 */
package it.unibo.oop.lab.mvc;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 */
public class ControllerImpl implements Controller {

    private final List<String> list;
    private String strToPrint;

    protected ControllerImpl() {
        list = new LinkedList<String>();
    }
    /**
     * {@inheritDoc}}
     */
    public void setString(final String s) throws IOException {
        if (s == null) {
            throw new IOException();
        }
        this.strToPrint = s;
        list.add(s);
    }

    /**
     * {@inheritDoc}
     */
    public String getString() {
        return this.strToPrint;
    }

    /**
     * {@inheritDoc}
     */
    public List<String> historyOfPrint() {
        return new LinkedList<>(list);
    }

    /**
     * {@inheritDoc}
     */
    public void printString() throws IllegalStateException {
        if (this.getString() == null) {
            throw new IllegalArgumentException();
        }
        System.out.println(this.getString());
    }

}
